import { Injectable } from '@angular/core';
import { Especialdad } from '../modelos/especialdad'

@Injectable({
  providedIn: 'root'
})
export class EspecialidadesService {
  especialidades:Especialdad[]=[
    {
      titulo:'Dr Jan Patterson',
      primerp:'Especilista en obstetro ginocologia',
      subtitulo:'"SOy muy felix"',
      segundop:'con el nuevo sistema de medicos puedo hacer cobros con tarjeta de credito'
    }
  ];
  constructor() { }

  getEsp(){
    return this.especialidades;
  }
}
