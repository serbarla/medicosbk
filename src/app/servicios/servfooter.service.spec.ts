import { TestBed } from '@angular/core/testing';

import { ServfooterService } from './servfooter.service';

describe('ServfooterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServfooterService = TestBed.get(ServfooterService);
    expect(service).toBeTruthy();
  });
});
