import { Beneficio } from './../modelos/beneficio';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServbeneficioService {

modelo:Beneficio[]=[
  {
    imagen:'placeholder.webp',
    titulo:'titulo1',
    contenido:'contenido1',
  },
  {
    imagen:'placeholder.webp',
    titulo:'titulo2',
    contenido:'contenido2',
  },
  {
    imagen:'placeholder.webp',
    titulo:'titulo3',
    contenido:'contenido3',
  },
  {
    imagen:'placeholder.webp',
    titulo:'titulo4',
    contenido:'contenido4',
  },
  {
    imagen:'placeholder.webp',
    titulo:'titulo5',
    contenido:'contenido5',
  },
  {
    imagen:'placeholder.webp',
    titulo:'titulo6',
    contenido:'contenido6',
  },
  {
    imagen:'placeholder.webp',
    titulo:'titulo7',
    contenido:'contenido7',
  },
 
  
  

]

  constructor() { }

getmodelo(){
  return this.modelo;
}


}
