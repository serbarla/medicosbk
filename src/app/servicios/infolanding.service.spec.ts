import { TestBed } from '@angular/core/testing';

import { InfolandingService } from './infolanding.service';

describe('InfolandingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfolandingService = TestBed.get(InfolandingService);
    expect(service).toBeTruthy();
  });
});
