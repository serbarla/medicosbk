import { Injectable } from '@angular/core';
import {Infolading} from "../modelos/infolading";
@Injectable({
  providedIn: 'root'
})
export class InfolandingService {
  datos:Infolading[]=[
  {
    titulo: "titulo1",
    parrafo: "parrafo1",
    boton1: "ingrese",
    boton2: "mas informacion",
  }
  ]

  constructor() {}
  getdatos(){
    return this.datos;
  }
}
