import { Injectable } from '@angular/core';
import { Footer } from "../modelos/footer";

@Injectable({
  providedIn: 'root'
})
export class ServfooterService {

  modelo:Footer[]=[
    {
      titulo: 'titulo',
      titulo2: 'titulo2',
      titulo3: 'titulo3',
      titulo4: 'titulo4',
      contenido: 'contenido',
      contenido2: 'contenido2',
      contenido3: 'contenido3',
      contenido4: 'contenido4',
      contenido5: 'contenido5',
      contenido6: 'contenido6',
      contenido7: 'contenido7',
      contenido8: 'contenido8',
      boton: 'boton',
    },

  ]
  constructor() { }


  getmodelo(){
    return this.modelo;
  }
}
