import { Component, OnInit } from '@angular/core';
import { ServfooterService } from "../../servicios/servfooter.service";
import { Footer } from "../../modelos/footer";
import { faInstagram , faTwitter , faYoutube , faWhatsapp} from "@fortawesome/free-brands-svg-icons";
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faInstagram = faInstagram;
  faTwitter = faTwitter;
  faYoutube = faYoutube;
  faWhatsapp = faWhatsapp;

  serv:Footer[];
  constructor(private servicio:ServfooterService) { 
    this.serv=this.servicio.getmodelo();

  }

  ngOnInit() {
  }

}
