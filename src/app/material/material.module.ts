import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button'; 
import {MatInputModule} from '@angular/material/input'; 
import {MatStepperModule} from '@angular/material/stepper';
import { FontAwesomeModule} from '@fortawesome/angular-fontawesome'; 
import {MatBadgeModule} from '@angular/material/badge';
import {MatIconModule} from '@angular/material/icon';

var materialmodules = [
  MatButtonModule,
  MatInputModule,
  MatStepperModule,
  MatBadgeModule,
  MatIconModule
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    materialmodules,
    FontAwesomeModule,
  ],
  exports:[
    materialmodules,
    FontAwesomeModule,
  ]
})
export class MaterialModule { }
