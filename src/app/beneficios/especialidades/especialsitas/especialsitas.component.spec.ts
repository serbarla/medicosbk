import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecialsitasComponent } from './especialsitas.component';

describe('EspecialsitasComponent', () => {
  let component: EspecialsitasComponent;
  let fixture: ComponentFixture<EspecialsitasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecialsitasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecialsitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
