import { EspecialidadesService } from './../../../servicios/especialidades.service';
import { Especialdad } from './../../../modelos/especialdad';
import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-especialsitas',
  templateUrl: './especialsitas.component.html',
  styleUrls: ['./especialsitas.component.scss']
})
export class EspecialsitasComponent implements OnInit {
  @Input() reverse:string='';
  retornos:Especialdad[];
  constructor(private especialidades:EspecialidadesService) { 
    this.retornos = this.especialidades.getEsp();
  }

  ngOnInit() {
  }

}
