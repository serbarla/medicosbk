import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetabeneficiosComponent } from './tarjetabeneficios.component';

describe('TarjetabeneficiosComponent', () => {
  let component: TarjetabeneficiosComponent;
  let fixture: ComponentFixture<TarjetabeneficiosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetabeneficiosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetabeneficiosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
