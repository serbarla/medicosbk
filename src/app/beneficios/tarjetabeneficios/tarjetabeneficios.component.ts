import { Component, OnInit } from '@angular/core';
import { ServbeneficioService } from "../../servicios/servbeneficio.service";
import { Beneficio } from './../../modelos/beneficio';

@Component({
  selector: 'app-tarjetabeneficios',
  templateUrl: './tarjetabeneficios.component.html',
  styleUrls: ['./tarjetabeneficios.component.scss']
})
export class TarjetabeneficiosComponent implements OnInit {
  boxA:string = "box-top-left";
  boxB:string = "box-top-center";
tituloA:string = "tituloA";
contenidoA:string = "contenidoA";
tituloB:string = "tituloB";
contenidoB:string = "contenidoB";

  // boxG:string = "box-radiusA";
  // boxH:string = "box-radiusB";

  serv:Beneficio[];
  constructor(private servicio:ServbeneficioService) { 
    this.serv=this.servicio.getmodelo();
  }

  ngOnInit() {
  }

}
