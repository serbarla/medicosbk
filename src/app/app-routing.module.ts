import { HomeComponent } from './landing/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EspecialidadesComponent} from "../app/beneficios/especialidades/especialidades.component";
import {TarjetabeneficiosComponent} from "../app/beneficios/tarjetabeneficios/tarjetabeneficios.component";
const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'contactanos', component: TarjetabeneficiosComponent },
  { path: "especialidad", component: EspecialidadesComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
