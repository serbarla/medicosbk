import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './landing/home/home.component';
import { MaterialModule } from './material/material.module';

import{ FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TarjetabeneficiosComponent } from './beneficios/tarjetabeneficios/tarjetabeneficios.component';
import { EspecialidadesComponent } from './beneficios/especialidades/especialidades.component';
import { EspecialsitasComponent } from './beneficios/especialidades/especialsitas/especialsitas.component';
import { FooterComponent } from './footer/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TarjetabeneficiosComponent,
    EspecialidadesComponent,
    EspecialsitasComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
