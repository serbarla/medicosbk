import { Component, OnInit, HostBinding } from '@angular/core';
import {InfolandingService } from "../../servicios/infolanding.service";
import {Infolading} from "../../modelos/infolading";
import {Menues} from "../../modelos/menu";
import {MenusService} from "../../servicios/menus.service";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  datos:Infolading[];
  menus:Menues[];
  constructor(private dato:InfolandingService, private menu:MenusService ) {
    this.datos=this.dato.getdatos();
    this.menus=this.menu.getbotones();
   }

  ngOnInit() {
  }
  toggleTheme(){
  }

}
